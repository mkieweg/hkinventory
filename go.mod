module gitlab.com/mkieweg/hkinventory

go 1.14

require (
	github.com/brutella/hc v1.2.2
	github.com/eclipse/paho.mqtt.golang v1.2.0
)
