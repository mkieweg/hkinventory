package mqtt

import (
	"github.com/brutella/hc/log"
	libmqtt "github.com/eclipse/paho.mqtt.golang"
	"strings"
)

type Dispatcher struct {
	client     libmqtt.Client
	deviceMap  map[string]Accessory
	rxChannels []chan []byte
	txChannels []chan []byte
	rx         chan libmqtt.Message
}

func NewDispatcher(client libmqtt.Client) *Dispatcher {
	return &Dispatcher{
		client:    client,
		deviceMap: make(map[string]Accessory),
		rx:        make(chan libmqtt.Message, 100),
	}
}

func (d *Dispatcher) Subscribe(topic string, acc Accessory) {
	d.deviceMap[topic] = acc
	go func() {
		d.client.Subscribe(topic, 0, func(client libmqtt.Client, msg libmqtt.Message) {
			log.Debug.Println("Waiting for message...")
			d.rx <- msg
		})
	}()
}

func (d *Dispatcher) Publish(publishChannel chan string) {
	go func() {
		for {
			log.Debug.Println("Waiting for message...")
			msg := <-publishChannel
			log.Debug.Printf("Message: %s\n", msg)
			splitstring := strings.Split(msg, ":")
			topic := splitstring[0]
			message := splitstring[1]
			d.client.Publish(topic, 0, false, message)
		}
	}()
}

func (d *Dispatcher) Listen() {
	for {
		msg := <-d.rx
		log.Debug.Printf("Topic:\t%v\nPayload:\t%v", msg.Topic(), msg.Payload())
		mqttAccessory := d.deviceMap[msg.Topic()]
		mqttAccessory.HandleMessage(msg)
	}
}
