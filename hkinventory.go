package main

import (
	"encoding/json"
	"flag"
	"github.com/brutella/hc"
	"github.com/brutella/hc/accessory"
	"github.com/brutella/hc/log"
	"gitlab.com/mkieweg/hkinventory/mqtt"
	"io/ioutil"
	"os"
	"sync"
)

func main() {
	username := flag.String("username", "mqttuser", "mqtt user")
	password := flag.String("password", "mqttpassword", "mqtt password")
	uri := flag.String("uri", "tcp://192.168.2.250:1883", "mqtt broker uri")
	cid := flag.String("cid", "mqtt-client", "mqtt client id")
	debug := flag.Bool("debug", false, "debug logging")
	config := flag.String("config", "/root/accessories.json", "path to the config file")
	flag.Parse()
	if *debug {
		log.Debug.Enable()
	}
	log.Info.Enable()

	f, err := os.OpenFile("/var/log/homekit.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Info.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	log.Info.SetOutput(f)

	accessories := make([]mqtt.Accessory, 0)
	bridge := *mqtt.NewMQTTBridge()
	var wg sync.WaitGroup

	accs, err := os.Open(*config)
	if err != nil {
		log.Info.Fatalf("Error opening file: %v", err)
	}
	byteValue, err := ioutil.ReadAll(accs)
	if err != nil {
		log.Info.Fatalf("Error reading json: %v", err)
	}

	var infos []mqtt.Info
	if err := json.Unmarshal(byteValue, &infos); err != nil {
		log.Info.Fatalf("Error unmarshaling json: %v", err)
	}

	client := bridge.Register(*username, *password, *cid, *uri)
	dispatcher := mqtt.NewDispatcher(client)
	publishChannel := make(chan string)

	plainAccs := make([]*accessory.Accessory, 0)

	for _, info := range infos {
		acc := mqtt.NewMqttAccessory(info, publishChannel)
		accessories = append(accessories, *acc)
		if info.DeviceType != accessory.TypeLightbulb {
			for _, topic := range info.Topics {
				dispatcher.Subscribe(topic, *acc)
			}
		}
		plainAccs = append(plainAccs, acc.Accessory)
	}
	dispatcher.Publish(publishChannel)

	hkbridge := accessory.NewBridge(accessory.Info{
		Name:         "MQTT Bridge",
		SerialNumber: "1312",
	})
	t, err := hc.NewIPTransport(hc.Config{Pin: "11223344"}, hkbridge.Accessory, plainAccs...)
	if err != nil {
		log.Debug.Fatalln(err)
	}
	go dispatcher.Listen()

	hc.OnTermination(func() {
		<-t.Stop()
	})
	wg.Add(1)
	go Start(t, &wg)
	wg.Wait()
}

func Start(t hc.Transport, wg *sync.WaitGroup) {
	defer wg.Done()
	t.Start()
}
